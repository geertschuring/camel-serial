package nl.juniverse.rfid.exception;

@SuppressWarnings("serial")
public class NotASerialCommPortException extends Exception {

	public NotASerialCommPortException() {
	}
	
	public NotASerialCommPortException(String message) {
		super(message);
	}
	
	public NotASerialCommPortException(String message, Throwable cause) {
		super(message, cause);
	}
	
}
