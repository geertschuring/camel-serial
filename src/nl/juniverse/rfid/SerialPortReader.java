package nl.juniverse.rfid;

import gnu.io.CommPortIdentifier;
import gnu.io.NoSuchPortException;
import gnu.io.PortInUseException;
import gnu.io.SerialPort;
import gnu.io.SerialPortEvent;
import gnu.io.SerialPortEventListener;
import gnu.io.UnsupportedCommOperationException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Set;
import java.util.TooManyListenersException;

import nl.juniverse.rfid.exception.NotASerialCommPortException;

public class SerialPortReader {
	
	private Set<SerialPort> usedSerialPorts = new HashSet<>();
	private Set<SerialPortReaderListener> listeners = new HashSet<>();
	
	/**
	 * Initialize the <code>SerialPortReader</code> without selecting a port will make the <code>SerialPortReader</code>
	 * scan and connect to all available Serial Comm ports. It will simply ignore any port that is either already in use
	 * or not a Serial port.
	 */
	@SuppressWarnings("rawtypes")
	public SerialPortReader() {
		Enumeration commPorts = CommPortIdentifier.getPortIdentifiers();
		if (!commPorts.hasMoreElements()) {
			throw new IllegalStateException("No Comm ports found!");
		}
		while (commPorts.hasMoreElements()) {
			CommPortIdentifier portIdentifier = null;
			try {
				portIdentifier = (CommPortIdentifier) commPorts.nextElement();
				openSerialCommPort(portIdentifier);
			} catch (PortInUseException e) {
				System.out.println("Ignoring CommPort "+portIdentifier.getName()+" because it is already in use!");
			} catch (NotASerialCommPortException e) {
				System.out.println("Ignoring CommPort "+portIdentifier.getName()+" because it is not a Serial port!");
			} catch (UnsupportedCommOperationException e) {
				System.out.println("Ignoring CommPort "+portIdentifier.getName()+" because it could not be initialized using the default settings!");
			}
		}
	}
	
	/**
	 * Initialize the <code>SerialPortReader</code> with a single portname or a comma separated list of portnames
	 * @param portNames one or more portnames, separated by a comma
	 * @throws NoSuchPortException Thrown when one of the given ports cannot be located on the system 
	 * @throws PortInUseException Thrown when one of the given ports cannot be opened because it is already in use
	 * @throws NotASerialCommPortException Thrown when a given port is not a Serial CommPort
	 * @throws UnsupportedCommOperationException Thrown when a given port could not be initialized with the standard configuration (2400 baud, 8 data bits, 1 stop bit, 0 parity bits) 
	 */
	public SerialPortReader(String portNames) throws NoSuchPortException, PortInUseException, NotASerialCommPortException, UnsupportedCommOperationException {
		this(portNames.split(","));
	}
	
	/**
	 * Initialize the <code>SerialPortReader</code> with a variable amount of <code>String</code> portnames
	 * @param portNames one or more portnames
	 * @throws NoSuchPortException Thrown when one of the given ports cannot be located on the system 
	 * @throws PortInUseException Thrown when one of the given ports cannot be opened because it is already in use
	 * @throws NotASerialCommPortException Thrown when a given port is not a Serial CommPort
	 * @throws UnsupportedCommOperationException Thrown when a given port could not be initialized with the standard configuration (2400 baud, 8 data bits, 1 stop bit, 0 parity bits)
	 */
	public SerialPortReader(String... portNames) throws NoSuchPortException, PortInUseException, NotASerialCommPortException, UnsupportedCommOperationException {
		for (String portName : portNames) {
			openSerialCommPort(portName);
		}
	}
	
	
	private void openSerialCommPort(String portName) throws NoSuchPortException, PortInUseException, NotASerialCommPortException, UnsupportedCommOperationException {
		openSerialCommPort(CommPortIdentifier.getPortIdentifier(portName));
	}

	private void openSerialCommPort(CommPortIdentifier portIdentifier) throws PortInUseException, NotASerialCommPortException, UnsupportedCommOperationException {
	
		if (portIdentifier.getPortType() != CommPortIdentifier.PORT_SERIAL) {
			throw new NotASerialCommPortException(portIdentifier.getName() + " is not a Serial port");
		}
		
		try {
			final SerialPort serialPort = (SerialPort) portIdentifier.open(this.getClass().getName(), 2000); //TODO: figure out what these arguments mean
			System.out.println("Using port " + serialPort.getName());
			usedSerialPorts.add(serialPort);
			serialPort.setSerialPortParams(2400, SerialPort.DATABITS_8, SerialPort.STOPBITS_1, SerialPort.PARITY_NONE); //TODO: make these settings configurable
			serialPort.addEventListener(new SerialPortEventListener() {
				private BufferedReader br = new BufferedReader(new InputStreamReader(serialPort.getInputStream()));
				public void serialEvent(SerialPortEvent event) {
					try {
						String input = br.readLine();
						if (!input.isEmpty()) {
							tellListeners(serialPort.getName(), input);	
						}
					} catch (IOException e) {
						e.printStackTrace(); //FIXME: Do something sensible with this exception
					}
				}
			});
			serialPort.notifyOnDataAvailable(true);	
		} catch (TooManyListenersException e) {
			//TODO: handle exception in meaningful way
			e.printStackTrace();
		} catch (IOException e) {
			//TODO: handle exception in meaningful way
			e.printStackTrace();
		}
	}
	
	public void close() {
		for (SerialPort serialPort : usedSerialPorts) {
			System.out.println("Closing port: "+serialPort.getName());
			serialPort.close();
		}
	}
	
	public void addListener(SerialPortReaderListener listener) {
		listeners.add(listener);
	}
	
	public void removeListener(SerialPortReaderListener listener) {
		listeners.remove(listener);
	}
	
	private void tellListeners(String portName, String input) {
		for (SerialPortReaderListener listener : listeners) {
			listener.processLine(portName, input);
		}
	}
}
