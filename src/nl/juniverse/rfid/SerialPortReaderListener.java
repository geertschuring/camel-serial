package nl.juniverse.rfid;

public interface SerialPortReaderListener {

	void processLine(String portName, String line);
}
