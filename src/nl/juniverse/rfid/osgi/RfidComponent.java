package nl.juniverse.rfid.osgi;

import gnu.io.PortInUseException;
import gnu.io.UnsupportedCommOperationException;

import java.io.IOException;
import java.util.TooManyListenersException;

import nl.juniverse.rfid.SerialPortReader;
import nl.juniverse.rfid.SerialPortReaderListener;

import aQute.bnd.annotation.component.Activate;
import aQute.bnd.annotation.component.Component;
import aQute.bnd.annotation.component.Deactivate;

@Component
public class RfidComponent {
	
	private SerialPortReader reader;
	
	@Activate
	public void start() throws PortInUseException, TooManyListenersException, UnsupportedCommOperationException, IOException {
		System.out.println("Creating new SerialPortReader");
		reader = new SerialPortReader();
		reader.addListener(new SerialPortReaderListener() {
			public void processLine(String portName, String line) {
				System.out.println(portName + ": " + line);
			}
		});
	}
	
	@Deactivate
	void stop () {
		System.out.println("Closing SerialPortReader");
		reader.close();
	}
}